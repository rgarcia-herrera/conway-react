# Conway's Game of Life

## Didactic re-write using React

<https://en.wikipedia.org/wiki/Conway's_Game_of_Life>

![screenshot](screenshot.png)

## `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

