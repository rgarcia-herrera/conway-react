import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


function count_neighbors(hood, i, j) {
    var neighbors = 0;
    var N;
    var M;

    for (var n=i-1; n <= i+1; n++) {
	for (var m=j-1; m <= j+1; m++) {
	    // toroidal universe
	    // wrap around i
	    if (n === -1) {
		N = hood.length - 1;
	    } else if (n === hood.length) {
		N = 0
	    } else {
		N = n
	    }

	    // wrap around j
	    if (m === -1) {
		M = hood[i].length - 1;
	    } else if (m === hood[i].length) {
		M = 0;
	    } else {
		M = m;
	    }
	    neighbors += hood[N][M];
	}
    }
    return neighbors - hood[i][j]; // don't count self as neighbor!
}


function conway(cell, neighbors) {
    // Any live cell with fewer than two live neighbours dies, as if by underpopulation.
    if (cell === 1 && neighbors < 2) {
	return 0
    }
    // Any live cell with two or three live neighbours lives on to the next generation.
    if (cell === 1 && (neighbors === 2 || neighbors === 3)) {
	return 1;
    }
    // Any live cell with more than three live neighbours dies, as if by overpopulation.
    if (cell === 1 && neighbors > 3) {
	return 0;
    }

    // Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
    if (cell === 0 && neighbors === 3) {
	return 1;
    } else {
	return 0;
    }
}


function Square(props) {
    var fill;
    if (props.value === 1) {
	fill = 'black';
    } else{
	fill = 'white';
    }
    return (
	<button className="square"
	onClick={props.onClick}
	style={{background: fill}}></button>
    );
}

class Board extends React.Component {

    constructor(props) {
	super(props);
	const size = 70;
	const squares = [];
	for (let i=0; i < size; i++) {
	    squares[i] = Array(size).fill(0);
	}

	this.state = {
	    squares: squares,
	    size: size,
	    play: false,
	};
    }

    handleClick(i, j) {
	const squares = this.state.squares.slice();

	if (squares[i][j] === 0) {
	    squares[i][j] = 1;
	} else {
	    squares[i][j] = 0;
	}

	this.setState({
	    squares: squares,
	});
    }

    randomize() {
	const squares = this.state.squares.slice();
	
	for (let i=0; i < this.state.size; i++) {
	    for (let j=0; j < this.state.size; j++) {
		squares[i][j] = Math.random() <= 0.5 ? 0 : 1;
	    }
	}
	this.setState({
	    squares: squares,
	    });
    }

    next_gen() {
	const present = this.state.squares.slice();
	const future = [];
	for (let i=0; i < present.length; i++) {
	    future[i] = Array(present[i].length).fill(0);
	}
	
	for (let i=0; i < present.length; i++) {
	    for (let j=0; j < present.length; j++) {
		future[i][j] = conway(present[i][j],
				      count_neighbors(present,
						      i, j));
	    }
	}
	this.setState({
	    squares: future,
	});
    }


    toggle_play() {
	this.setState({
	    play: !this.state.play,
	});
    }

    maybe_play() {
	if (this.state.play) {
	    this.next_gen();
	}
    }
    
    componentDidMount() {
	this.intervalId = setInterval(this.maybe_play.bind(this), 600);
    }
    
    componentWillUnmount(){
	clearInterval(this.intervalId);
    }
    
    renderSquare(i, j, value) {
	return <Square value={value}
		       key={i+'_'+j}
		       onClick={() => this.handleClick(i, j)} />;
    }

    renderRow(i) {
	var items = [];
	for (var j=0; j < this.state.size; j++) {
	    items.push(this.renderSquare(i, j, this.state.squares[i][j]));
	}
	return <div key={i} className="board-row">{items}</div>
    }

    renderTable() {
	var items = [];
	for (var i=0; i < this.state.size; i++) {
	    items.push(this.renderRow(i));
	}
	return items;
    }

    render() {

	return (
		<div>
		{this.renderTable()}
		<div>
		  <button onClick={() => this.randomize()}>randomize</button>
  		  <button onClick={() => this.next_gen()}>next gen</button>
		  <button onClick={() => this.toggle_play()}>toggle play</button>		
		</div>
		</div>
	);
    }
}

class Game extends React.Component {

    render() {
	return (
	    <div className="game">
	    <div className="game-board">
	    <Board />
	    </div>
	    </div>
	);
    }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
